package test.com.poker;

import com.poker.cards.Card;

import com.poker.cards.RankEnum;
import com.poker.cards.SuitEnum;
import org.junit.Assert;
import junit.framework.TestCase;
import org.junit.Test;

public class CardTest extends TestCase {

    @Test
    public void testConstructor() {
        Card card = new Card(RankEnum.ACE, SuitEnum.HEARTS);
        Assert.assertEquals(card.toString(), "Ah");
    }
}